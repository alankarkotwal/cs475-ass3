#ifndef _HNODE_HPP_
#define _HNODE_HPP_

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
#include "glm/mat4x4.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"


#include "gl_framework.hpp"


const int FIXED = 0x0;
const int XROT = 0x1;
const int YROT = 0x2;
const int ZROT = 0x4;
const int XTRANS = 0x8;
const int YTRANS = 0x10;
const int ZTRANS = 0x20;

namespace csX75	 { 

	// A simple class that represents a node in the hierarchy tree
	class HNode {
		//glm::vec4 * vertices;
		//glm::vec4 * colors;
		GLfloat tx,ty,tz,rx,ry,rz,sx,sy,sz,shininess;

		std::size_t vertex_buffer_size, color_buffer_size, norm_buffer_size, tex_buffer_size;

		GLuint num_vertices;
		GLuint vao,vbo;
        GLuint myTex;

		glm::mat4 rotation;
		glm::mat4 translation;
		glm::mat4 scaling;
		
		std::vector<HNode*> children;
		HNode* parent;

		int movement_mask;

		void update_matrices();

	  public:
		HNode (HNode*, GLuint, glm::vec4*,  glm::vec4*, std::size_t, std::size_t, GLuint, glm::vec4*, glm::vec2*, std::size_t, std::size_t, GLfloat, int);
		//HNode (HNode* , glm::vec4*,  glm::vec4*,GLfloat,GLfloat,GLfloat,GLfloat,GLfloat,GLfloat);

		void add_child(HNode*);
		void render();
		void change_parameters(GLfloat,GLfloat,GLfloat,GLfloat,GLfloat,GLfloat);
		void scale(GLfloat, GLfloat, GLfloat);
		void render_tree();
		void inc_rx();
		void inc_ry();
		void inc_rz();
		void dec_rx();
		void dec_ry();
		void dec_rz();
		void inc_tx();
		void inc_ty();
		void inc_tz();
		void dec_tx();
		void dec_ty();
		void dec_tz();
	};

	glm::mat4* multiply_stack(std::vector <glm::mat4> );
};	


struct ModifiableNodes {
	csX75::HNode *body, *leg1, *leg2, *hand1, *hand2, *hand1upper, *hand2upper,*hand1lower,*hand2lower,
                 *leg1upper,*leg2upper,*leg1lower,*leg2lower, *head1, *head2, *head3, *head4, *head5, *foot1,
                 *foot2, *foot1side1 ,*foot1side2, *foot2side1, *foot2side2;

};

struct ModifiableNodesR2 {
	csX75::HNode *body, *head, *leg1joint, *leg2joint, *leg1, *leg2, *foot1, *foot2, *plugarm, *plug;
};

class MovableParams {
		public: 
		GLfloat tx,ty,tz,rx,ry,rz;
		bool enabled;
		glm::mat4 as_matrix();
};
#endif