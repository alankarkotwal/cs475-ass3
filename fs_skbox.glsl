#version 330

in vec3 texcoords;
uniform samplerCube cube_texture;
out vec4 frag_colour;

void main () {
    frag_colour = texture(cube_texture, texcoords);
    if(frag_colour.r == 0.0 && frag_colour.g == 0.0 && frag_colour.b == 0.0 && frag_colour.a == 1.0 )
    frag_colour = vec4(0.529, 0.808, 1.0, 1.0);
}