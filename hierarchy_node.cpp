#include "hierarchy_node.hpp"

#include <iostream>

extern GLuint vPosition, vColor, vNorm, vTex, shiny, uModelMatrix;
extern std::vector<glm::mat4> matrixStack;

namespace csX75
{

	HNode::HNode(HNode* a_parent, GLuint num_v, glm::vec4* a_vertices, glm::vec4* a_colours, std::size_t v_size, std::size_t c_size, GLuint _myTex, glm::vec4* a_norms, glm::vec2* a_texs, std::size_t n_size, std::size_t t_size, GLfloat _shininess, int mask){

		num_vertices = num_v;
		vertex_buffer_size = v_size;
		color_buffer_size = c_size;
        norm_buffer_size = n_size;
        tex_buffer_size = t_size;
        shininess = _shininess;
		// initialize vao and vbo of the object;


		//Ask GL for a Vertex Attribute Objects (vao)
		glGenVertexArrays (1, &vao);
		//Ask GL for aVertex Buffer Object (vbo)
		glGenBuffers (1, &vbo);

		//bind them
		glBindVertexArray (vao);
		glBindBuffer (GL_ARRAY_BUFFER, vbo);
        
        // Get texture
        myTex = _myTex;
		
		glBufferData (GL_ARRAY_BUFFER, vertex_buffer_size + color_buffer_size + norm_buffer_size + tex_buffer_size, NULL, GL_STATIC_DRAW);
		glBufferSubData( GL_ARRAY_BUFFER, 0, vertex_buffer_size, a_vertices );
		glBufferSubData( GL_ARRAY_BUFFER, vertex_buffer_size, color_buffer_size, a_colours );
        glBufferSubData( GL_ARRAY_BUFFER, vertex_buffer_size + color_buffer_size, norm_buffer_size, a_norms );
        glBufferSubData( GL_ARRAY_BUFFER, vertex_buffer_size + color_buffer_size + norm_buffer_size, tex_buffer_size, a_texs );

		//setup the vertex array as per the shader
		glEnableVertexAttribArray( vPosition );
		glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );

		glEnableVertexAttribArray( vColor );
		glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertex_buffer_size) );

        glEnableVertexAttribArray( vNorm );
        glVertexAttribPointer( vNorm, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertex_buffer_size + color_buffer_size) );
        
        glEnableVertexAttribArray( vTex );
        glVertexAttribPointer( vTex, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertex_buffer_size + color_buffer_size + norm_buffer_size) );

		// set parent

		if(a_parent == NULL){
			parent = NULL;
		}
		else{
			parent = a_parent;
			parent->add_child(this);
		}
		movement_mask = mask;
		//initial parameters are set to 0;

		tx=ty=tz=rx=ry=rz=0;
		sx=sy=sz=1;
		update_matrices();
	}

	void HNode::update_matrices(){

		rotation = glm::rotate(glm::mat4(1.0f), glm::radians(rx), glm::vec3(1.0f,0.0f,0.0f));
		rotation = glm::rotate(rotation, glm::radians(ry), glm::vec3(0.0f,1.0f,0.0f));
		rotation = glm::rotate(rotation, glm::radians(rz), glm::vec3(0.0f,0.0f,1.0f));

		translation = glm::translate(glm::mat4(1.0f),glm::vec3(tx,ty,tz));
		scaling = glm::scale(glm::mat4(1.0f), glm::vec3(sx,sy,sz));

	}

	void HNode::add_child(HNode* a_child){
		children.push_back(a_child);

	}

	void HNode::change_parameters(GLfloat atx, GLfloat aty, GLfloat atz, GLfloat arx, GLfloat ary, GLfloat arz){
		tx = atx;
		ty = aty;
		tz = atz;
		rx = arx;
		ry = ary;
		rz = arz;

		update_matrices();
	}

	// Mostly should only be used by root nodes for easy resizing
	void HNode::scale(GLfloat asx, GLfloat asy, GLfloat asz){
		sx = asx; 
		sy = asy;
		sz = asz;

		update_matrices();
	}

	void HNode::render(){

		//matrixStack multiply
		glm::mat4* ms_mult = multiply_stack(matrixStack);
		glUniformMatrix4fv(uModelMatrix, 1, GL_FALSE, glm::value_ptr(*ms_mult));
        glUniform1f(shiny, shininess);
		glBindVertexArray (vao);
        glBindTexture(GL_TEXTURE_2D, myTex);
		glDrawArrays(GL_TRIANGLES, 0, num_vertices);

		// for memory 
		delete ms_mult;

	}

	void HNode::render_tree(){
		
		matrixStack.push_back(scaling);
		matrixStack.push_back(translation);
		matrixStack.push_back(rotation);

		render();
		for(int i=0;i<children.size();i++){
			children[i]->render_tree();
		}
		matrixStack.pop_back();
		matrixStack.pop_back();
		matrixStack.pop_back();

	}

	void HNode::inc_rx(){
		if(movement_mask & XROT) {
			rx+=10;
			update_matrices();
		}
	}


	void HNode::inc_ry(){
		if(movement_mask & YROT) {
			ry+=10;
			update_matrices();
		}
	}

	void HNode::inc_rz(){
		if(movement_mask & ZROT) {
			rz+=10;
			update_matrices();
		}
	}

	void HNode::dec_rx(){
		if(movement_mask & XROT) {
			rx-=10;
			update_matrices();
		}
	}

	void HNode::dec_ry(){
		if(movement_mask & YROT) {
			ry-=10;
			update_matrices();
		}
	}

	void HNode::dec_rz(){
		if(movement_mask & ZROT) {
			rz-=10;
			update_matrices();
		}
	}

	void HNode::inc_tx(){
		if(movement_mask & XTRANS) {
			tx+=0.2;
			update_matrices();
		}
	}


	void HNode::inc_ty(){
		if(movement_mask & YTRANS) {
			ty+=0.2;
			update_matrices();
		}
	}

	void HNode::inc_tz(){
		if(movement_mask & ZTRANS) {
			tz+=0.2;
			update_matrices();
		}
	}

	void HNode::dec_tx(){
		if(movement_mask & XTRANS) {
			tx-=0.2;
			update_matrices();
		}
	}

	void HNode::dec_ty(){
		if(movement_mask & YTRANS) {
			ty-=0.2;
			update_matrices();
		}
	}

	void HNode::dec_tz(){
		if(movement_mask & ZTRANS) {
			tz-=0.2;
			update_matrices();
		}
	}

	glm::mat4* multiply_stack(std::vector<glm::mat4> matStack){
		glm::mat4* mult;
		mult = new glm::mat4(1.0f);
	
		for(int i=0;i<matStack.size();i++){
			*mult = (*mult) * matStack[i];
		}	

		return mult;
	}

}

glm::mat4 MovableParams::as_matrix() {
		glm::mat4 mat = glm::rotate(glm::mat4(1.0f), glm::radians(rx), glm::vec3(1.0f,0.0f,0.0f));
		mat = glm::rotate(mat, glm::radians(ry), glm::vec3(0.0f,1.0f,0.0f));
		mat = glm::rotate(mat, glm::radians(rz), glm::vec3(0.0f,0.0f,1.0f));

		mat = glm::translate(mat,glm::vec3(tx,ty,tz));
		return mat;
}